package de.sopra.sopragui.desktop;

import de.unisaarland.sopra.executables.Client;

/**
 * Erstellt von Sven am 29.09.2016.
 */
public class ClientThread extends Thread {

    private Client client;

    public ClientThread(String[] args, Client client) {
        this.client = client;
    }

    public static void startClientThread(String[] args, Client client) {
        new ClientThread(args, client).start();
    }

    @Override
    public void run() {
        client.run();
    }
}
