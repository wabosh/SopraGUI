package de.sopra.sopragui.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import de.sopra.sopragui.GameClass;
import de.unisaarland.sopra.executables.Client;
import de.unisaarland.sopra.executables.views.View;

public class DesktopLauncher {
	public static void main (String[] arg) {
		Client client = View.setup(arg);
        GameClass gameClass = new GameClass(client);
        client.setView(gameClass);
		ClientThread.startClientThread(arg, client);

		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 1024;
		config.height = 768;
		new LwjglApplication(gameClass, config);
	}
}
