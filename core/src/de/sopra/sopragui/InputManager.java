package de.sopra.sopragui;

import de.unisaarland.sopra.Direction;
import de.unisaarland.sopra.model.Position;
import de.unisaarland.sopra.sendables.commands.*;
import de.unisaarland.sopra.sendables.events.BurnedEvent;
import de.unisaarland.sopra.sendables.events.DoneActingEvent;
import de.unisaarland.sopra.sendables.events.MovedEvent;
import de.unisaarland.sopra.sendables.events.SingedEvent;

public class InputManager {

    private int id;

    private Command nextCommand;

    private Direction direction;
    private Position position;
    private PositionState positionState;
    private CurrentCommand currentCommand;

    private ResettableCountDownLatch countDownLatch;

    public InputManager(int id) {
        this.id = id;
        direction = null;
        position = new Position(0, 0);
        positionState = PositionState.EMPTY;
        currentCommand = CurrentCommand.NONE;
        countDownLatch = new ResettableCountDownLatch(1);
    }

    public Command getCommand() throws InterruptedException {
        countDownLatch.await();
        countDownLatch.reset();
        Command command = nextCommand;
        clearCommands();
        return command;
    }

    private void clearCommands() {
        // Loesche alles
        direction = null;
        positionState = PositionState.EMPTY;
        currentCommand = CurrentCommand.NONE;
        nextCommand = null;
    }

    public void sendInput(char key) {
        System.out.println(key);
        switch (key) {
            // Commands
            case 109:
                currentCommand = CurrentCommand.M;
                break;
            case 114:
                currentCommand = CurrentCommand.R;
                break;
            case 102:
                currentCommand = CurrentCommand.F;
                executeCommand();
                break;
            case 8:
                currentCommand = CurrentCommand.BS;
                executeCommand();
                break;

            // Directions
            case 113:
                direction = Direction.NORTH_WEST;
                if(currentCommand == CurrentCommand.NONE) {
                    currentCommand = CurrentCommand.M;
                }
                executeCommand();
                break;
            case 97:
                direction = Direction.WEST;
                if(currentCommand == CurrentCommand.NONE) {
                    currentCommand = CurrentCommand.M;
                }
                executeCommand();
                break;
            case 121:
                direction = Direction.SOUTH_WEST;
                if(currentCommand == CurrentCommand.NONE) {
                    currentCommand = CurrentCommand.M;
                }
                executeCommand();
                break;
            case 99:
                direction = Direction.SOUTH_EAST;
                if(currentCommand == CurrentCommand.NONE) {
                    currentCommand = CurrentCommand.M;
                }
                executeCommand();
                break;
            case 100:
                direction = Direction.EAST;
                if(currentCommand == CurrentCommand.NONE) {
                    currentCommand = CurrentCommand.M;
                }
                executeCommand();
                break;
            case 101:
                direction = Direction.NORTH_EAST;
                if(currentCommand == CurrentCommand.NONE) {
                    currentCommand = CurrentCommand.M;
                }
                executeCommand();
                break;

            case 13:
                executeCommand();
                break;
        }
    }

    private void executeCommand() {
        switch (currentCommand) {
            case M:
                if (direction != null) {
                    nextCommand = new MoveCommand(id, direction, new MovedEvent(id, direction));
                    countDownLatch.countDown();
                }
                break;
            case BS:
                nextCommand = new DoneActingCommand(id, new DoneActingEvent(id));
                countDownLatch.countDown();
                break;
            case F:
                nextCommand = new BurnCommand(id, new BurnedEvent(id));
                countDownLatch.countDown();
                break;
            case R:
                if (direction != null) {
                    nextCommand = new SingeCommand(id, direction, new SingedEvent(id, direction));
                    countDownLatch.countDown();
                }
        }
    }

    @Override
    public String toString() {
        return currentCommand.toString();
    }

    private enum PositionState {
        EMPTY,
        ONE,
        TWO
    }

    private enum CurrentCommand {
        NONE,
        M, // MOVE
        R, // SINGE
        F, // BURN
        BS // DONE_ACTING
    }

}