package de.sopra.sopragui;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import de.unisaarland.sopra.executables.Client;
import de.unisaarland.sopra.executables.views.View;
import de.unisaarland.sopra.sendables.commands.Command;
import de.unisaarland.sopra.sendables.commands.DoneActingCommand;
import de.unisaarland.sopra.sendables.events.DoneActingEvent;
import de.unisaarland.sopra.sendables.events.Event;

public class GameClass extends ApplicationAdapter implements View, InputProcessor {

    private Client client;

    private SpriteBatch batch;
    private SpriteBatch hudBatch;
    private ShapeRenderer shapeRenderer;
    private OrthographicCamera camera;
    private Map map;
    private Hud hud;
    private CreatureManager creatureManager;
    private InputManager inputManager;

    private int cameraTranslateX;
    private int cameraTranslateY;

    public GameClass(Client client) {
        this.client = client;
    }

    @Override
    public void create() {
        Gdx.input.setInputProcessor(this);
        batch = new SpriteBatch();
        hudBatch = new SpriteBatch();
        shapeRenderer = new ShapeRenderer();
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.translate(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);
        camera.update();
        map = new Map(client.getCm().getArena().getArray());
        hud = new Hud(client.getCm().getCreatureControllerFromId(client.getId()));
        creatureManager = new CreatureManager(client.getCm().getSortedPlayers(), client.getCm().getSortedBoars(), client.getCm().getSortedFairies(), client.getCm().getArena().getArray().length);
        inputManager = new InputManager(client.getId());
    }

    @Override
    public void render() {
        update(Gdx.graphics.getDeltaTime());
        batch.setProjectionMatrix(camera.combined);
        Gdx.gl.glClearColor(0.1f, 0.1f, 0.1f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        map.render(batch);
        creatureManager.render(batch);
        batch.end();
        hud.render(hudBatch, shapeRenderer);
    }

    private void update(float delta) {
        if (cameraTranslateX != 0 || cameraTranslateY != 0) {
            camera.translate(delta * cameraTranslateX * Constants.CAMERA_HORIZONTAL_SPEED, delta * cameraTranslateY * Constants.CAMERA_VERTICAL_SPEED);
            camera.update();
        }
    }

    @Override
    public void dispose() {
        batch.dispose();
    }

    @Override
    public void notifyEvent(Event event) {
        System.out.println(event.toString());
        map.updateMap(client.getCm().getArena().getArray());
        creatureManager.update(client.getCm().getSortedPlayers(), client.getCm().getSortedBoars(), client.getCm().getSortedFairies());
    }

    @Override
    public Command getNextCommand() {
        Command command = null;
        try {
            command = inputManager.getCommand();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if(command.check(client.getCm())) {
            return command;
        }
        return getNextCommand();
    }

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Keys.LEFT) {
            cameraTranslateX = -1;
        } else if (keycode == Keys.RIGHT) {
            cameraTranslateX = 1;
        } else if (keycode == Keys.UP) {
            cameraTranslateY = 1;
        } else if (keycode == Keys.DOWN) {
            cameraTranslateY = -1;
        }
        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        if (keycode == Keys.LEFT) {
            cameraTranslateX = 0;
        }
        if (keycode == Keys.RIGHT) {
            cameraTranslateX = 0;
        }
        if (keycode == Keys.UP) {
            cameraTranslateY = 0;
        }
        if (keycode == Keys.DOWN) {
            cameraTranslateY = 0;
        }
        return true;
    }

    @Override
    public boolean keyTyped(char character) {
        inputManager.sendInput(character);
        return true;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
