package de.sopra.sopragui;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import de.unisaarland.sopra.model.FieldType;

/**
 * Erstellt von Sven am 29.09.2016.
 */
public class Map {

    private Texture bush;
    private Texture curse;
    private Texture grass;
    private Texture heal;
    private Texture hill;
    private Texture ice;
    private Texture lava;
    private Texture rock;
    private Texture swamp;
    private Texture tree;
    private Texture water;

    public FieldType[][] fieldTypes;

    public Map(FieldType[][] fieldTypes) {
        this.fieldTypes = fieldTypes;

        bush = new Texture("fields/bush.png");
        curse = new Texture("fields/curse.png");
        grass = new Texture("fields/grass.png");
        heal = new Texture("fields/heal.png");
        hill = new Texture("fields/hill.png");
        ice = new Texture("fields/ice.png");
        lava = new Texture("fields/lava.png");
        rock = new Texture("fields/rock.png");
        swamp = new Texture("fields/swamp.png");
        tree = new Texture("fields/tree.png");
        water = new Texture("fields/water.png");
    }

    public void render(SpriteBatch batch) {
        int maxHeight = fieldTypes.length;
        for(int i = 0; i < fieldTypes.length; i++) {
            // Falls reihe gerade, offset um die haelfte der TexturWeite
            int offsetX = 0;
            if(i % 2 == 0) {
                offsetX = Constants.MAP_TILE_WIDTH / 2;
            }
            for(int j = 0; j < fieldTypes[0].length; j++) {
                batch.draw(getTextureFromFieldType(fieldTypes[Math.abs(i - maxHeight + 1)][j]), offsetX + j * Constants.MAP_TILE_WIDTH, i * Constants.MAP_TILE_HEIGHT * 0.75f);
            }
        }
    }

    private Texture getTextureFromFieldType(FieldType fieldType) {
        switch (fieldType) {
            case BUSH:
                return bush;
            case CURSE:
                return curse;
            case GRASS:
                return grass;
            case HEAL:
                return heal;
            case HILL:
                return hill;
            case ICE:
                return ice;
            case LAVA:
                return lava;
            case ROCK:
                return rock;
            case SWAMP:
                return swamp;
            case TREE:
                return tree;
            case WATER:
                return water;
            default:
                return rock;
        }

    }

    public void updateMap(FieldType[][] fieldTypes) {
        this.fieldTypes = fieldTypes;
    }

}
