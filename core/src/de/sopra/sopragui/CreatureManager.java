package de.sopra.sopragui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import de.unisaarland.sopra.MonsterType;
import de.unisaarland.sopra.model.NonPlayerController;
import de.unisaarland.sopra.model.PlayerController;
import de.unisaarland.sopra.model.Position;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Erstellt von Sven am 30.09.2016.
 */
public class CreatureManager {

    private LinkedList<PlayerController> playerControllers;
    private LinkedList<NonPlayerController> boarControllers;
    private LinkedList<NonPlayerController> fairyControllers;

    private Texture dummy;
    private Texture ifrit;

    private Texture boar;
    private Texture fairy;

    private FreeTypeFontGenerator generator;
    private FreeTypeFontGenerator.FreeTypeFontParameter parameter;
    private BitmapFont hpFont;
    private BitmapFont nameFont;

    private int maxHeight;

    public CreatureManager(Queue<PlayerController> playerControllers, Queue<NonPlayerController> boarControllers, Queue<NonPlayerController> fairyControllers, int maxHeight) {
        this.playerControllers = (LinkedList<PlayerController>) playerControllers;
        this.boarControllers = (LinkedList<NonPlayerController>) boarControllers;
        this.fairyControllers = (LinkedList<NonPlayerController>) fairyControllers;
        this.maxHeight = maxHeight;

        dummy = new Texture("character/dummy.png");
        ifrit = new Texture("character/ifrit.png");

        boar = new Texture("character/boar.png");
        fairy = new Texture("character/fairy.png");

        // Font-zeugs
        generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/font.ttf"));
        parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = Constants.CREATURE_HP_FONT_SIZE;
        hpFont = generator.generateFont(parameter);
        generator.dispose();

    }

    public void render(SpriteBatch batch) {
        for (PlayerController pc : playerControllers) {
            float x = pc.getPosition().getY() * Constants.MAP_TILE_WIDTH / 2 + pc.getPosition().getX() * Constants.MAP_TILE_WIDTH;
            float y = Math.abs(pc.getPosition().getY() + 1 - maxHeight) * Constants.MAP_TILE_HEIGHT * 0.75f;
            batch.draw(getTextureByType(pc.getPCType()), x, y);
            hpFont.draw(batch, pc.getCreature().getHp() + "/" + pc.getCreature().getMaxHp(), x, y);
        }
        for (NonPlayerController npc : boarControllers) {
            batch.draw(boar, npc.getPosition().getY() * Constants.MAP_TILE_WIDTH / 2 + npc.getPosition().getX() * Constants.MAP_TILE_WIDTH, Math.abs(npc.getPosition().getY() + 1 - maxHeight) * Constants.MAP_TILE_HEIGHT * 0.75f);
        }
        for (NonPlayerController npc : fairyControllers) {
            batch.draw(fairy, npc.getPosition().getY() * Constants.MAP_TILE_WIDTH / 2 + npc.getPosition().getX() * Constants.MAP_TILE_WIDTH, Math.abs(npc.getPosition().getY() + 1 - maxHeight) * Constants.MAP_TILE_HEIGHT * 0.75f);
        }
    }

    public void update(Queue<PlayerController> playerControllers, Queue<NonPlayerController> boarControllers, Queue<NonPlayerController> fairyControllers) {
        this.playerControllers = (LinkedList<PlayerController>) playerControllers;
        this.boarControllers = (LinkedList<NonPlayerController>) boarControllers;
        this.fairyControllers = (LinkedList<NonPlayerController>) fairyControllers;
    }

    private Texture getTextureByType(MonsterType monsterType) {
        if (monsterType == MonsterType.IFRIT) {
            return ifrit;
        }
        return dummy;
    }

}
