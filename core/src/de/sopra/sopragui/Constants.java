package de.sopra.sopragui;

/**
 * Erstellt von Sven am 29.09.2016.
 */
public class Constants {

    // Map Related
    public static final int MAP_TILE_WIDTH = 64;
    public static final int MAP_TILE_HEIGHT = 64;

    // Camera Related
    public static final int CAMERA_HORIZONTAL_SPEED = 200;
    public static final int CAMERA_VERTICAL_SPEED = 200;

    // HUD Related
    public static final int HP_BAR_X = 16;
    public static final int HP_BAR_Y = 16;
    public static final int HP_BAR_WIDTH = 128;
    public static final int HP_BAR_HEIGTH = 16;
    public static final int HP_FONT_SIZE = 14;

    // CreatureManager
    public static final int CREATURE_HP_FONT_SIZE = 10;

}
