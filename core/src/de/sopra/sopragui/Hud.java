package de.sopra.sopragui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import de.unisaarland.sopra.model.CreatureController;
import de.unisaarland.sopra.model.PlayerController;

/**
 * Created by Sven on 30.09.2016.
 */
public class Hud {

    private PlayerController ownController;

    private FreeTypeFontGenerator generator;
    private FreeTypeFontParameter parameter;

    private BitmapFont hpFont;

    public Hud(CreatureController ownController) {
        this.ownController = (PlayerController)ownController;

        generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/font.ttf"));
        parameter = new FreeTypeFontParameter();

        parameter.size = Constants.HP_FONT_SIZE;
        hpFont = generator.generateFont(parameter);

        generator.dispose();

    }

    public void render(SpriteBatch batch, ShapeRenderer shapeRenderer) {
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        // Hintergrundflaeche
        shapeRenderer.setColor(0.2f, 0.2f, 0.2f, 0.9f);
        shapeRenderer.rect(0, 0, Gdx.graphics.getWidth(), 128);
        // HP-Balken-Hintergrund
        shapeRenderer.setColor(0.2f, 0, 0, 1);
        shapeRenderer.rect(Constants.HP_BAR_X, Constants.HP_BAR_Y, Constants.HP_BAR_WIDTH, Constants.HP_BAR_HEIGTH);
        // HP-Balken
        shapeRenderer.setColor(0.6f, 0, 0, 1);
        shapeRenderer.rect(Constants.HP_BAR_X, Constants.HP_BAR_Y, ownController.getCreature().getHp() / ownController.getCreature().getMaxHp() * Constants.HP_BAR_WIDTH, Constants.HP_BAR_HEIGTH);
        shapeRenderer.end();
        batch.begin();
        // HP Text
        hpFont.draw(batch, ownController.getCreatureHp() + " / " + ownController.getCreature().getMaxHp(), Constants.HP_BAR_X, Constants.HP_BAR_Y);
        // TODO Add drawing of hp etc
        batch.end();
    }

}
